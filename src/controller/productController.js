import { Product } from "../schema/model.js";

export let createProduct = async (req, res) => {
  // console.log("*****************************");
  // console.log(process.env.PORT);
  let data = req.body;
  // save data to Product
  try {
    let result = await Product.create(data);
    res.json({
      success: true,
      message: "product created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readProduct = async (req, res) => {
  try {
    let result = await Product.find({}); //it gives all data
    // let result = await Product.find({ name: "ram" });
    // let result = await Product.find({ name: "ram", age: 29 });

    // 15 17
    // let result = await Product.find({ age: { $lte: 17, $gte: 15 } });

    // let result = await Product.find({ name: /^.{5}$/ });

    // let result = await Product.find({}).sort("name");
    // let result = await Product.find({}).sort("name");
    // let result = await Product.find({}).sort("-isMarried");
    // let result = await Product.find({}).select("name age -_id");

    // let result = await Product.find({}).limit("10").skip("20")

    res.json({
      success: true,
      message: "product read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateProduct = async (req, res) => {
  let productId = req.params.productId;
  let data = req.body;

  try {
    let result = await Product.findByIdAndUpdate(productId, data);

    res.json({
      success: true,
      message: "Product updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readProductDetails = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findById(productId);

    res.json({
      success: true,
      message: "Product read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteProductDetails = async (req, res) => {
  let productId = req.params.productId;

  try {
    let result = await Product.findByIAndDelete(productId);

    res.json({
      success: true,
      message: "Product deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
