import { Review } from "../schema/model.js";

export let createReview = async (req, res) => {
  let data = req.body;
  // save data to Review
  try {
    let result = await Review.create(data);
    res.json({
      success: true,
      message: "review created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readReview = async (req, res) => {
  try {
    let result = await Review.find({})
      .populate("productId", "name price")
      .populate("userId", "name email");

    res.json({
      success: true,
      message: "review read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateReview = async (req, res) => {
  let reviewId = req.params.reviewId;
  let data = req.body;

  try {
    let result = await Review.findByIdAndUpdate(reviewId, data);

    res.json({
      success: true,
      message: "Review updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readReviewDetails = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findById(reviewId);

    res.json({
      success: true,
      message: "Review read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteReviewDetails = async (req, res) => {
  let reviewId = req.params.reviewId;

  try {
    let result = await Review.findByIAndDelete(reviewId);

    res.json({
      success: true,
      message: "Review deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
