import { Student } from "../schema/model.js";

export let createStudent = async (req, res) => {
  let data = req.body;
  // save data to Student
  try {
    let result = await Student.create(data);
    res.json({
      success: true,
      message: "student created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readStudent = async (req, res) => {
  try {
    let result = await Student.find({}); //it gives all data
    // let result = await Student.find({ name: "ram" });
    // let result = await Student.find({ name: "ram", age: 29 });

    // 15 17
    // let result = await Student.find({ age: { $lte: 17, $gte: 15 } });

    // let result = await Student.find({ name: /^.{5}$/ });

    // let result = await Student.find({}).sort("name");
    // let result = await Student.find({}).sort("name");
    // let result = await Student.find({}).sort("-isMarried");
    // let result = await Student.find({}).select("name age -_id");

    // let result = await Student.find({}).limit("10").skip("20")

    res.json({
      success: true,
      message: "student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateStudent = async (req, res) => {
  let studentId = req.params.studentId;
  let data = req.body;

  try {
    let result = await Student.findByIdAndUpdate(studentId, data);

    res.json({
      success: true,
      message: "Student updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readStudentDetails = async (req, res) => {
  let studentId = req.params.studentId;

  try {
    let result = await Student.findById(studentId);

    res.json({
      success: true,
      message: "Student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudentDetails = async (req, res) => {
  let studentId = req.params.studentId;

  try {
    let result = await Student.findByIAndDelete(studentId);

    res.json({
      success: true,
      message: "Student deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
