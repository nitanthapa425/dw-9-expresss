// import { Trainee } from "../schema/model.js";

import { Trainee } from "../schema/model.js";

export let createTrainee = async (req, res) => {
  let data = req.body;
  // save data to Trainee
  try {
    let result = await Trainee.create(data);
    res.json({
      success: true,
      message: "trainee created successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readTrainee = async (req, res) => {
  let query = req.query;

  let brake = req.query.brake;
  let page = req.query.page;

  try {
    // let result = await Trainee.find(query);
    let result = await Trainee.find({})
      .skip((page - 1) * brake)
      .limit(brake);
    res.json({
      success: true,
      message: "trainee read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateTrainee = async (req, res) => {
  let traineeId = req.params.traineeId;
  let data = req.body;

  try {
    let result = await Trainee.findByIdAndUpdate(traineeId, data);

    res.json({
      success: true,
      message: "Trainee updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readTraineeDetails = async (req, res) => {
  let traineeId = req.params.traineeId;

  try {
    let result = await Trainee.findById(traineeId);

    res.json({
      success: true,
      message: "Trainee read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteTraineeDetails = async (req, res) => {
  let traineeId = req.params.traineeId;

  try {
    let result = await Trainee.findByIAndDelete(traineeId);

    res.json({
      success: true,
      message: "Trainee deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

// 1+1=2
// "1"+1="11"
// "1"*1 =0
// "a"-1
