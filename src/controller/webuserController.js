// import { Webuser } from "../schema/model.js";

import { Webuser } from "../schema/model.js";
import bcrypt from "bcrypt";

export let createWebuser = async (req, res) => {
  // save data to Webuser

  try {
    let data = req.body; //{name:"nitan",password:"abc"}
    let password = data.password;
    let hashPassword = await bcrypt.hash(password, 10);
    data = {
      ...data,
      password: hashPassword,
    };

    let result = await Webuser.create(data);
    res.json({
      success: true,
      message: "webuser created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuser = async (req, res) => {
  try {
    // let result = await Webuser.find(query);
    let result = await Webuser.find({});

    res.json({
      success: true,
      message: "webuser read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateWebuser = async (req, res) => {
  let webuserId = req.params.webuserId;
  let data = req.body;

  try {
    let result = await Webuser.findByIdAndUpdate(webuserId, data);

    res.json({
      success: true,
      message: "Webuser updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;

  try {
    let result = await Webuser.findById(webuserId);

    res.json({
      success: true,
      message: "Webuser read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteWebuserDetails = async (req, res) => {
  let webuserId = req.params.webuserId;

  try {
    let result = await Webuser.findByIAndDelete(webuserId);

    res.json({
      success: true,
      message: "Webuser deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let loginWebuser = async (req, res) => {
  let email = req.body.email;
  let password = req.body.password;

  try {
    let user = await Webuser.findOne({ email: email });
    if (user === null) {
      res.json({
        success: false,
        message: "Email or Password does not match.",
      });
    } else {
      let databasePassword = user.password;
      let isValidPassword = await bcrypt.compare(password, databasePassword);

      console.log(isValidPassword);

      if (isValidPassword) {
        res.json({
          success: true,
          message: "login successfully.",
        });
      } else {
        res.json({
          success: false,
          message: "Email or Password does not match.",
        });
      }
    }
  } catch (error) {
    res.json({
      success: false,
      message: "Email or Password does not match.",
    });
  }
};

// email , password
// get email and password
// if email exist
// if email exist we check password match
//if match res => login successfull.
//else Email or Password does not match

// 1+1=2
// "1"+1="11"
// "1"*1 =0
// "a"-1
