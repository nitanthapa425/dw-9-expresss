import { Router } from "express";

let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000
  .post((req, res) => {
    // console.log("body data", req.body);
    // console.log("query data", req.query);

    res.json("home post");
    // res.json("nitan");
  });

firstRouter.route("/name").post((req, res) => {
  res.json("name post");
});

firstRouter.route("/a/:country/b/:name").post((req, res) => {
  console.log(req.params);

  res.json("hello");
});

export default firstRouter;

//
// url = localhost:8000
// methode = post

// clg "home post"

//pass data from postman
// get data in backend
