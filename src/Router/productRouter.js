import { Router } from "express";
import {
  createProduct,
  deleteProductDetails,
  readProduct,
  readProductDetails,
  updateProduct,
} from "../controller/productController.js";
// import {
//   createProduct,
//   deleteProductDetails,
//   readProduct,
//   readProductDetails,
//   updateProduct,
// } from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/") //localhost:8000/products
  .post(createProduct)
  .get(readProduct);

// let result = await Product.findByIdAndDelete(productId);

productRouter
  .route("/:productId") //localhost:8000/products/any
  .patch(updateProduct)
  .get(readProductDetails)
  .delete(deleteProductDetails);

export default productRouter;

// create   .create(data)
// read all   .find({})
// read specific  .findById(id)
// update specific .findByIdAndUpdate(id,data)
// delete specific  .findByIdAndDelete(id)
