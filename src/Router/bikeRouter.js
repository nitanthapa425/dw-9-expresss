import { Router } from "express";

let bikeRouter = Router();

bikeRouter
  .route("/") //localhost:8000/bike
  .post(
    (req, res, next) => {
      console.log("i am middleware 1");
      next("a");
    },
    (err, req, res, next) => {
      console.log("i am middleware 2");
      next([1, 2]);
    },
    (err, req, res, next) => {
      console.log("i am middleware 3");
      res.json("output");
    },
    (req, res, next) => {
      console.log("i am middleware 4");
    }
  )
  .get(() => {
    console.log("bike get");
  })
  .patch(() => {
    console.log("bike patch");
  })
  .delete(() => {
    console.log("bike delete");
  });

export default bikeRouter;
