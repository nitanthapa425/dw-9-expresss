import { Router } from "express";
import {
  createWebuser,
  deleteWebuserDetails,
  loginWebuser,
  readWebuser,
  readWebuserDetails,
  updateWebuser,
} from "../controller/webuserController.js";

let webuserRouter = Router();

webuserRouter
  .route("/") //localhost:8000/webusers
  .post(createWebuser)
  .get(readWebuser);

// let result = await Webuser.findByIdAndDelete(webuserId);

webuserRouter.route("/login").post(loginWebuser);

webuserRouter
  .route("/:webuserId") //localhost:8000/webusers/any
  .patch(updateWebuser)
  .get(readWebuserDetails)
  .delete(deleteWebuserDetails);

export default webuserRouter;
