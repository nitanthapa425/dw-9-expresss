import { Router } from "express";
import {
  createStudent,
  deleteStudentDetails,
  readStudent,
  readStudentDetails,
  updateStudent,
} from "../controller/studentController.js";

let studentRouter = Router();

studentRouter
  .route("/") //localhost:8000/students
  .post(createStudent)
  .get(readStudent);

// let result = await Student.findByIdAndDelete(studentId);

studentRouter
  .route("/:studentId") //localhost:8000/students/any
  .patch(updateStudent)
  .get(readStudentDetails)
  .delete(deleteStudentDetails);

export default studentRouter;

// create   .create(data)
// read all   .find({})
// read specific  .findById(id)
// update specific .findByIdAndUpdate(id,data)
// delete specific  .findByIdAndDelete(id)
