import { Router } from "express";
import {
  createReview,
  deleteReviewDetails,
  readReview,
  readReviewDetails,
  updateReview,
} from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter
  .route("/") //localhost:8000/reviews
  .post(createReview)
  .get(readReview);

// let result = await Review.findByIdAndDelete(reviewId);

reviewRouter
  .route("/:reviewId") //localhost:8000/reviews/any
  .patch(updateReview)
  .get(readReviewDetails)
  .delete(deleteReviewDetails);

export default reviewRouter;

// create   .create(data)
// read all   .find({})
// read specific  .findById(id)
// update specific .findByIdAndUpdate(id,data)
// delete specific  .findByIdAndDelete(id)
