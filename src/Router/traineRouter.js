import { Router } from "express";
import { createTrainee, deleteTraineeDetails, readTrainee, readTraineeDetails, updateTrainee } from "../controller/traineeController.js";
// import {
//   createTrainee,
//   deleteTraineeDetails,
//   readTrainee,
//   readTraineeDetails,
//   updateTrainee,
// } from "../controller/traineeController.js";

let traineeRouter = Router();

traineeRouter
  .route("/") //localhost:8000/trainees
  .post(createTrainee)
  .get(readTrainee);

// let result = await Trainee.findByIdAndDelete(traineeId);

traineeRouter
  .route("/:traineeId") //localhost:8000/trainees/any
  .patch(updateTrainee)
  .get(readTraineeDetails)
  .delete(deleteTraineeDetails);

export default traineeRouter;

// create   .create(data)
// read all   .find({})
// read specific  .findById(id)
// update specific .findByIdAndUpdate(id,data)
// delete specific  .findByIdAndDelete(id)
