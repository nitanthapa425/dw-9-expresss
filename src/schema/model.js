// defining array is called model

import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./traineeSchema.js";
import webuserSchema from "./webUser.js";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Trainee = model("Trainee", traineeSchema);
export let Webuser = model("Webuser", webuserSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);

// model name must be singular and firstLetter capital
// match
