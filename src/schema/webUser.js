import { Schema } from "mongoose";

let webuserSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required."],
    // lowercase: true,
    // uppercase:true
    trim: true,
    minLength: 3,
    maxLength: 10,
    // name filed only alphabet , number
    validate: function (value) {
      let isValid = /^[a-zA-Z0-9]+$/.test(value);
      if (!isValid) {
        throw new Error("only alphabet and number are allowed");
      }
    },
  },
  email: {
    type: String,
    required: [true, "email field is required."],
    unique: true,
  },

  age: {
    type: Number,
    required: [true, "age field is required."],
    min: 18,
    max: 150,
    validate: (value) => {
      if (value === 35) {
        throw new Error("unable to register due to age factor");
      }
    },
  },
  password: {
    type: String,
    required: [true, "password field is required."],
  },
  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required."],
    // unique: true,
    validate: (value) => {
      // value => number 9849468999
      let strValue = String(value); //"9849468999"
      if (strValue.length !== 10) {
        throw new Error("Phone number must be exact 10 character long.");
      }
    },
  },
  isMarried: {
    type: Number,
    required: [true, "phoneNumber field is required."],
  },
  spouseName: {
    type: String,
    required: [
      function () {
        if (this.isMarried) {
          return true;
        } else {
          return false;
        }
      },
      "spouseName field is required.",
    ],
  },
  gender: {
    type: String,
    required: [true, "gender field is required."],
    default: "male",
  },
  dob: {
    type: Date,
    required: [true, "dob field is required."],
  },
  location: {
    country: {
      type: String,
      required: [true, "country field is required."],
    },
    exactLocation: {
      type: String,
      required: [true, "exactLocation field is required."],
    },
  },

  favTeacher: [
    {
      type: String,
      required: [true, "favTeacher field is required."],
    },
  ],

  favSubject: [
    {
      bookName: {
        type: String,
        required: [true, "bookName field is required."],
      },
      bookAuthor: {
        type: String,
        required: [true, "bookAuthor field is required."],
      },
    },
  ],
});

export default webuserSchema;

// design
// manipulation
// validation
