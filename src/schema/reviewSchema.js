//defining object is call schema

import { Schema } from "mongoose";

let reviewSchema = Schema({
  productId: {
    // type: String,
    required: [true, "productId field is required."],
    type: Schema.ObjectId,
    ref: "Product",
  },
  userId: {
    required: [true, "userId field is required."],
    type: Schema.ObjectId,
    ref: "Webuser",
  },
  rating: {
    type: Number,
    required: [true, "rating field is required."],
  },
  description: {
    type: String,
    required: [true, "description field is required."],
  },
});

export default reviewSchema;
