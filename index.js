// make express application
// assign port to that application
import express, { json } from "express";
import firstRouter from "./src/Router/firstRouter.js";
import bikeRouter from "./src/Router/bikeRouter.js";
import connectToMongoDB from "./src/databaseConnection/mongoDbConnection.js";
import studentRouter from "./src/Router/StudentRouter.js";
import traineeRouter from "./src/Router/traineRouter.js";
import webuserRouter from "./src/Router/webuserRouter.js";
import productRouter from "./src/Router/productRouter.js";
import reviewRouter from "./src/Router/reviewRouter.js";
import cors from "cors";
import bcrypt from "bcrypt";
import { config } from "dotenv";
import { port } from "./src/config.js";

import jwt from "jsonwebtoken";
let expressApp = express();
expressApp.use(json()); //always place this code at top of the router
expressApp.use(cors());

expressApp.listen(port, () => {
  console.log(`Express app is listening at port ${port}`);
});
connectToMongoDB();

expressApp.use("/", firstRouter);
expressApp.use("/bikes", bikeRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/webusers", webuserRouter);
expressApp.use("/products", productRouter);
expressApp.use("/reviews", reviewRouter);

// let password = "Password@123";

// let hashPassword = await bcrypt.hash(password, 10);  =>2

// let isValidPassword = await bcrypt.compare(
//   "$2b$10$PKeZF0fijbbqJazkQrcRte7HjUvNln2l.5G7w/F8RTBA9dOlplmKq",
//   "$2b$10$yTj4T0NhMNVT6.yywUBJHOH7j5NywxnsS5lxYuB9FeQ4JV/Ye9XQi"
// );
// console.log(isValidPassword);

/*
productId
userId
rating
description 

*/

// localhost:8000/webusers/login

//generate token

// let infoObject = {
//   id: "1234123412",
// };
// // generally we give id at infoObject

// let secretKey = "dw9";

// let expiryInfo = {
//   expiresIn: "365d",
// };

// let token =  jwt.sign(infoObject, secretKey, expiryInfo);
// console.log(token);

let token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQxMjM0MTIiLCJpYXQiOjE3MDE4MzE0NDAsImV4cCI6MTczMzM2NzQ0MH0.-am7dvkmJ48SxxhCX4l-zEU5v-YjjrEP5MrsUgfvyKA";

let infoObj = jwt.verify(token, "dw99");

console.log(infoObj);

// valid token
/* 
token must be made form the give secretkey
not expired
 */
