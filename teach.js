/* 

//regext searching




// for normal searching
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]


//for  regex searchin
[
   {name:"ni1t",age:29, isMarried:false},=
    {name:"sand2inip",age:25, isMarried:false},

    {name:"ni",age:26, isMarried:true},=
    {name:"ris3hav",age:20, isMarried:false},

    {name:"nitan",age:29, isMarried:true},=
    {name:"chhimi",age:15, isMarried:true},

    {name:"narendran",age:27, isMarried:false},
    {name:"Nitan",age:16, isMarried:false},

    {name:"nitanthapa",age:22, isMarried:false},=
]
find({name:"nitan"})//exact searching
find({name:/nitan/})// regex searching => not exact searching
find({name:/nitan/i})// regex searching => not exact searching
find({name:/^ni/})
find({name:/ni$/})
find({name:/ni/})



 

find(name:/nitan/i)
find(name:/ni/)
find(name:/^ni/)
find(name:/^ni/i)
find(name:/n$/i)
find(name:/^.{5}$/)// regex with length 5
find(name:/^n.*n$/) // regex starts with n and ends with n


find(name:/^(?=.*[a-zA-Z])(?=.*\d).+/)

find(name:/^(?=.*@)(?=.*_)|(?=.*_)(?=.*@)/)





//for array and object searching
[
    //searching
    //search according field
  
    {
      name: "nitan",
      location: {
        country: "nepal",
        exactLocation: "gagal",
      },
      favTeacher: ["bhishma", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "ram",
      location: {
        country: "china",
        exactLocation: "sikhu",
      },
      favTeacher: ["hari", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
    {
      name: "shyam",
      location: {
        country: "india",
        exactLocation: "bihar",
      },
      favTeacher: ["niti", "nitan", "ram"],
      favSubject: [
        {
          bookName: "C",
          bookAuthor: "nitan",
        },
        {
          bookName: "javascript",
          bookAuthor: "nitan",
        },
      ],
    },
  ];
  
  //find({name:"nitan"})
  // find({ "location.country": "nepal" });
  // find({favTeacher:"bhishma"})
  //find({"favSubject.bookAuthor":"nitan"})
  

  //find it determine which object to show or not
  //select
  //sort
  //limit
  //skip


  //for sorting
  [
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"c",age:40, isMarried:false},
  
]

find({age:{$gte:40}}).select("name")

//output
[
     {name:"ab"},
    {name:"ab"},
    {name:"b"},
    {name:"c"},

]






//number sorting work properly unlike javascript
// find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")

// //ascending sort  descending sort
// 

// find({}).sort("-name age")

// find({}).sort("age -name")


//skip
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},

]

find({}).skip("8")



//limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

find({}).skip("3").limit("5")

//outptu
[
  

]

find({}).limit("2")




//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[

 
 


]
find({}).skip("4")




// 100
10 limit("10")  skip("0")
10 limit("10")  skip("10")
10 limit ("10") skip("20")
10 limit ("10") skip
10
10
10
10
10
10



// find({}).limit("5").skip("2")

//this order works

//find , sort, select, skipt, limit





[
    {name:"nitan",age:29, isMarried:false},=  +
    {name:"sandip",age:25, isMarried:false},

    {name:"nitan",age:26, isMarried:true},=
    {name:"rishav",age:20, isMarried:false},

    {name:"nitan",age:29, isMarried:true},=  +
    {name:"chhimi",age:15, isMarried:true},

    {name:"narendra",age:27, isMarried:false}
    {name:"shidhant",age:16, isMarried:false},

    {name:"kriston",age:22, isMarried:false},      
]

localhost:8000/students?brake=2&page=5

brake=2
page=5


find({}).limit(2).skip(4)

find({}).skip(brake*(page-1)).limit(brake)







.find({}).limit(brake).skip(page*brake)

[
   {name:"nitan",age:29, isMarried:true},=  +
    {name:"chhimi",age:15, isMarried:true},
]



find({name:"nitan"})
find({name:"nitan",age:29})
find({age:27})
find({age:"27"})
find({age:22, isMarried:"false"})
in searching type does not matter

find({age:25})
find({age:{$gt:25}})
find({age:{$gte:25}})
find({age:{$lt:25}})
find({age:{$lte:25}})
find({name:{$ne:"nitan"}})

task 1
find age between 15 to 17
find({age:{$get:15, $lte:17}})

finding those whose name is nitan , ram , hari
find({name:{$in:["nitan","ram","hari"]}})
find({name:{$nin:["nitan","ram","hari"]}})

$or
$and

find({$or:[{name:"nitan"}, {name:"kriston"}]})
find({$or:[{name:"nitan", age:29}, {name:"kriston"}]})

find({$or:[{name:"nitan"}, {age:29}]})
find({$and:[{name:"nitan"}, {age:29}]})

find those User does not contain name nitan ram hari







*/

/* 

{
    "name": "c",
    "age": 33,
    "email": "abc@gmai",
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "isMarried": false,
    "spouseName": "lkjl",
    
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



}



frontend                         database
Passwrod@123                      Password@123
Password@123       asidfjaskdfj12#$22344          asidfjaskdfj12#$22344



*/
